package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiNumber {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//byte 	byteValue()
		outputTitle("byteValue()");
		//abstract double 	doubleValue()
		outputTitle("doubleValue()");
		//abstract float 	floatValue()
		outputTitle("floatValue()");
		//abstract int 	intValue()
		outputTitle("intValue()");
		//abstract long 	longValue()
		outputTitle("longValue()");
		//short 	shortValue()
		outputTitle("shortValue()");

	}

	private static void outputTitle(String title){
		System.out.println("★" + title + "★");
	}


}
