package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiString {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ


		/* コンストラクタ */

		//String()
		outputTitle("String()");
		//String(byte[] bytes)
		outputTitle("String(byte[] bytes)");
		//String(byte[] bytes, Charset charset)
		outputTitle("String(byte[] bytes, Charset charset)");
		//String(byte[] bytes, int offset, int length)
		outputTitle("String(byte[] bytes, int offset, int length)");
		//String(byte[] bytes, int offset, int length, Charset charset)
		outputTitle("String(byte[] bytes, int offset, int length, Charset charset)");
		//String(byte[] bytes, int offset, int length, String charsetName)
		outputTitle("String(byte[] bytes, int offset, int length, String charsetName)");
		//String(byte[] bytes, String charsetName)
		outputTitle("String(byte[] bytes, String charsetName)");
		//String(char[] value)
		outputTitle("String(char[] value)");
		//String(char[] value, int offset, int count)
		outputTitle("String(char[] value, int offset, int count)");
		//String(int[] codePoints, int offset, int count)
		outputTitle("String(int[] codePoints, int offset, int count)");
		//String(String original)
		outputTitle("String(String original)");
		//String(StringBuffer buffer)
		outputTitle("String(StringBuffer buffer)");
		//String(StringBuilder builder)
		outputTitle("String(StringBuilder builder)");

		/* メソッド */

		//char 	charAt(int index)
		outputTitle("charAt(int index)");
		//int 	codePointAt(int index)
		outputTitle("codePointAt(int index)");
		//int 	codePointBefore(int index)
		outputTitle("codePointBefore(int index)");
		//int 	codePointCount(int beginIndex, int endIndex)
		outputTitle("codePointCount(int beginIndex, int endIndex)");
		//int 	compareTo(String anotherString)
		outputTitle("compareTo(String anotherString)");
		//int 	compareToIgnoreCase(String str)
		outputTitle("compareToIgnoreCase(String str)");
		//String 	concat(String str)
		outputTitle("concat(String str)");
		//boolean 	contains(CharSequence s)
		outputTitle("contains(CharSequence s)");
		//boolean 	contentEquals(CharSequence cs)
		outputTitle("contentEquals(CharSequence cs)");
		//boolean 	contentEquals(StringBuffer sb)
		outputTitle("contentEquals(StringBuffer sb)");
		//static String 	copyValueOf(char[] data)
		outputTitle("copyValueOf(char[] data)");
		//static String 	copyValueOf(char[] data, int offset, int count)
		outputTitle("copyValueOf(char[] data, int offset, int count)");
		//boolean 	endsWith(String suffix)
		outputTitle("endsWith(String suffix)");
		//boolean 	equals(Object anObject)
		outputTitle("equals(Object anObject)");
		//boolean 	equalsIgnoreCase(String anotherString)
		outputTitle("equalsIgnoreCase(String anotherString)");
		//static String 	format(Locale l, String format, Object... args)
		outputTitle("format(Locale l, String format, Object... args)");
		//static String 	format(String format, Object... args)
		outputTitle("format(String format, Object... args)");
		//byte[] 	getBytes()
		outputTitle("getBytes()");
		//byte[] 	getBytes(Charset charset)
		outputTitle("getBytes(Charset charset)");
		//byte[] 	getBytes(String charsetName)
		outputTitle("getBytes(String charsetName)");
		//void 	getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
		outputTitle("getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)");
		//int 	hashCode()
		outputTitle("hashCode()");
		//int 	indexOf(int ch)
		outputTitle("indexOf(int ch)");
		//int 	indexOf(int ch, int fromIndex)
		outputTitle("indexOf(int ch, int fromIndex)");
		//int 	indexOf(String str)
		outputTitle("indexOf(String str)");
		//int 	indexOf(String str, int fromIndex)
		outputTitle("indexOf(String str, int fromIndex)");
		//String 	intern()
		outputTitle("intern()");
		//boolean 	isEmpty()
		outputTitle("isEmpty()");
		//int 	lastIndexOf(int ch)
		outputTitle("lastIndexOf(int ch)");
		//int 	lastIndexOf(int ch, int fromIndex)
		outputTitle("lastIndexOf(int ch, int fromIndex)");
		//int 	lastIndexOf(String str)
		outputTitle("lastIndexOf(String str)");
		//int 	lastIndexOf(String str, int fromIndex)
		outputTitle("lastIndexOf(String str, int fromIndex)");
		//int 	length()
		outputTitle("length()");
		//boolean 	matches(String regex)
		outputTitle("matches(String regex)");
		//int 	offsetByCodePoints(int index, int codePointOffset)
		outputTitle("offsetByCodePoints(int index, int codePointOffset)");
		//boolean 	regionMatches(boolean ignoreCase, int toffset, String other, int ooffset, int len)
		outputTitle("regionMatches(boolean ignoreCase, int toffset, String other, int ooffset, int len)");
		//boolean 	regionMatches(int toffset, String other, int ooffset, int len)
		outputTitle("regionMatches(int toffset, String other, int ooffset, int len)");
		//String 	replace(char oldChar, char newChar)
		outputTitle("replace(char oldChar, char newChar)");
		//String 	replace(CharSequence target, CharSequence replacement)
		outputTitle("replace(CharSequence target, CharSequence replacement)");
		//String 	replaceAll(String regex, String replacement)
		outputTitle("replaceAll(String regex, String replacement)");
		//String 	replaceFirst(String regex, String replacement)
		outputTitle("replaceFirst(String regex, String replacement)");
		//String[] 	split(String regex)
		outputTitle("split(String regex)");
		//String[] 	split(String regex, int limit)
		outputTitle("split(String regex, int limit)");
		//boolean 	startsWith(String prefix)
		outputTitle("startsWith(String prefix)");
		//boolean 	startsWith(String prefix, int toffset)
		outputTitle("startsWith(String prefix, int toffset)");
		//CharSequence 	subSequence(int beginIndex, int endIndex)
		outputTitle("subSequence(int beginIndex, int endIndex)");
		//String 	substring(int beginIndex)
		outputTitle("substring(int beginIndex)");
		//String 	substring(int beginIndex, int endIndex)
		outputTitle("substring(int beginIndex, int endIndex)");
		//char[] 	toCharArray()
		outputTitle("toCharArray()");
		//String 	toLowerCase()
		outputTitle("toLowerCase()");
		//String 	toLowerCase(Locale locale)
		outputTitle("toLowerCase(Locale locale)");
		//String 	toString()
		outputTitle("toString()");
		//String 	toUpperCase()
		outputTitle("toUpperCase()");
		//String 	toUpperCase(Locale locale)
		outputTitle("toUpperCase(Locale locale)");
		//String 	trim()
		outputTitle("trim()");
		//static String 	valueOf(boolean b)
		outputTitle("valueOf(boolean b)");
		//static String 	valueOf(char c)
		outputTitle("valueOf(char c)");
		//static String 	valueOf(char[] data)
		outputTitle("valueOf(char[] data)");
		//static String 	valueOf(char[] data, int offset, int count)
		outputTitle("valueOf(char[] data, int offset, int count)");
		//static String 	valueOf(double d)
		outputTitle("valueOf(double d)");
		//static String 	valueOf(float f)
		outputTitle("valueOf(float f)");
		//static String 	valueOf(int i)
		outputTitle("valueOf(int i)");
		//static String 	valueOf(long l)
		outputTitle("valueOf(long l)");
		//static String 	valueOf(Object obj)
		outputTitle("valueOf(Object obj)");


	}


	private static void outputTitle(String title){
		System.out.println("★" + title + "★");
	}

}
