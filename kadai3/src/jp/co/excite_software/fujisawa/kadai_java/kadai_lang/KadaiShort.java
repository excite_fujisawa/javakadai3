package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiShort {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//byte 	byteValue()
		outputTitle("byteValue()");
		//static int 	compare(short x, short y)
		outputTitle("compare(short x, short y)");
		//int 	compareTo(Short anotherShort)
		outputTitle("compareTo(Short anotherShort)");
		//static Short 	decode(String nm)
		outputTitle("decode(String nm)");
		//double 	doubleValue()
		outputTitle("doubleValue()");
		//boolean 	equals(Object obj)
		outputTitle("equals(Object obj)");
		//float 	floatValue()
		outputTitle("floatValue()");
		//int 	hashCode()
		outputTitle("hashCode()");
		//int 	intValue()
		outputTitle("intValue()");
		//long 	longValue()
		outputTitle("longValue()");
		//static short 	parseShort(String s)
		outputTitle("parseShort(String s)");
		//static short 	parseShort(String s, int radix)
		outputTitle("parseShort(String s, int radix)");
		//static short 	reverseBytes(short i)
		outputTitle("reverseBytes(short i)");
		//short 	shortValue()
		outputTitle("shortValue()");
		//String 	toString()
		outputTitle("toString()");
		//static String 	toString(short s)
		outputTitle("toString(short s)");
		//static Short 	valueOf(short s)
		outputTitle("valueOf(short s)");
		//static Short 	valueOf(String s)
		outputTitle("valueOf(String s)");
		//static Short 	valueOf(String s, int radix)
		outputTitle("valueOf(String s, int radix)");

	}

	private static void outputTitle(String title){
		System.out.println("★" + title + "★");
	}


}
