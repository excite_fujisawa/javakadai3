package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiInteger {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		/* コンストラクタ */
		Integer a = 123;
		output(a);
		Integer b = new Integer("-456");
		output(b);
		//int c = new Integer("a");
		//output(c);
		//Integer d = new Integer("456.45");
		//output(d);

		/* メソッド */
		//bitCount(int i)
		System.out.println("bitCount(int i)");
		output(Integer.bitCount(a));
		output(Integer.bitCount(b));

		//byteValue()
		System.out.println("byteValue()");
		byte bt = a.byteValue();
		System.out.println(bt);
		bt = b.byteValue();
		System.out.println(bt);

		//compare(int x, int y)
		System.out.println("compare(int x, int y)");
		output(Integer.compare(a, b));
		output(Integer.compare(a, a));

		//compareTo(Integer anotherInteger)
		System.out.println("compareTo(Integer anotherInteger)");
		output(a.compareTo(b));
		output(a.compareTo(a));

		//decode(String nm)
		System.out.println("decode(String nm)");
		String str = "123";
		output(Integer.decode(str));
		str = "-987654";
		output(Integer.decode(str));

		//doubleValue()
		System.out.println("doubleValue()");
		double db = a.doubleValue();
		System.out.println(Double.toString(db));
		db = b.doubleValue();
		System.out.println(Double.toString(db));

		//equals(Object obj)
		System.out.println("equals(Object obj)");
		System.out.println(a.equals(b));
		System.out.println(a.equals(str));
		System.out.println(a.equals(a));
		System.out.println(a.equals(123));

		//floatValue()
		System.out.println("floatValue()");
		float fl = a.floatValue();
		System.out.println(Float.toString(fl));
		fl = b.floatValue();
		System.out.println(Float.toString(fl));

		//getInteger(String nm)
		System.out.println("getInteger(String nm)");
		str = "123";
		//output(Integer.getInteger(str));
		str = "abc";
		//output(Integer.getInteger(str));

		//getInteger(String nm, int val) 使用用途不明のため割愛

		//getInteger(String nm, Integer val) 同上

		//hashCode() 同上

		//highestOneBit(int i) 同上

		//intValue() これ何の意味があるんだろう…IntegerからInt？
		System.out.println("intValue()");
		System.out.println(a.intValue());
		System.out.println(b.intValue());

		//longValue()
		System.out.println("longValue()");
		System.out.println(a.longValue());
		System.out.println(b.longValue());
		Integer c = 1234567890;
		Long ln = c.longValue();
		System.out.println(ln);

		//lowestOneBit(int i)
		System.out.println("lowestOneBit(int i)");
		output(Integer.lowestOneBit(a.intValue()));
		output(Integer.lowestOneBit(b.intValue()));

		//numberOfLeadingZeros(int i)
		System.out.println("numberOfLeadingZeros(int i)");
		output(Integer.numberOfLeadingZeros(a.intValue()));
		output(Integer.numberOfLeadingZeros(b.intValue()));

		//numberOfTrailingZeros(int i)
		System.out.println("numberOfTrailingZeros(int i)");
		output(Integer.numberOfTrailingZeros(a.intValue()));
		output(Integer.numberOfTrailingZeros(b.intValue()));

		//parseInt(String s)
		System.out.println("parseInt(String s)");
		str = "123";
		output(Integer.parseInt(str));
		str = "abc654987";
		//output(Integer.parseInt(str));
		str = "-987";
		output(Integer.parseInt(str));

		//parseInt(String s, int radix)
		System.out.println("paresInt(String s, int radix)");
		str = "-12345";
//		output(Integer.parseInt(str, 2));
//		output(Integer.parseInt(str, 3));

		//reverse(int i)
		System.out.println("reverse(int i)");
		output(Integer.reverse(a));
		output(Integer.reverse(b));

		//reverseBytes(int i)
		System.out.println("reverseBytes(int i)");
		output(Integer.reverseBytes(a));
		output(Integer.reverseBytes(b));

		//rotateLeft(int i, int distance)
		System.out.println("rotateLeft(int i, int distance)");
		output(Integer.rotateLeft(a, 2));
		output(Integer.rotateLeft(a, 16));

		//rotateRight(int i, int distance)
		System.out.println("rotateRight(int i, int distance)");
		output(Integer.rotateRight(a, 2));
		output(Integer.rotateRight(1, 3));

		//shortValue()
		System.out.println("shortValue");
		short sh = a.shortValue();
		System.out.println(Short.toString(sh));
		sh = b.shortValue();
		System.out.println(Short.toString(sh));

		//signum(int i)
		System.out.println("signum(int i)");
		output(Integer.signum(a));
		output(Integer.signum(b));

		//toBinaryString(int i)
		System.out.println("toBinaryString() 2進数");
		System.out.println(Integer.toBinaryString(a));

		//toHexString(int i)
		System.out.println("toHexString(int i) 16進数");
		System.out.println(Integer.toHexString(a))
		;
		//toOctalString(int i)
		System.out.println("toOctalString(int i) 8進数");
		System.out.println(Integer.toOctalString(a));

		//toString(int i) メソッドoutput(int i)で使用しているため割愛

		//valueOf(int i)
		System.out.println("valueOf(int i)");
		output(Integer.valueOf(a));
		output(Integer.valueOf(b));

		//valueOf(String s)
		System.out.println("valueOf(String s)");
		//output(Integer.valueOf("a"));
		output(Integer.valueOf("123456"));

		//valueOf(String s, int radix)
		System.out.println("valueOf(String s, int radix)");
		output(Integer.valueOf("abc", 16));
		output(Integer.valueOf("12", 8));

	}

	private static void output(Integer i){
		System.out.println(Integer.toString(i));
	}

}
