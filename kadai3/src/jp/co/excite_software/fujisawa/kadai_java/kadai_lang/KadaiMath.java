package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiMath {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//static double 	abs(double a)
		outputTitle("abs(double a)");
		//static float 	abs(float a)
		outputTitle("abs(float a)");
		//static int 	abs(int a)
		outputTitle("abs(int a)");
		//static long 	abs(long a)
		outputTitle("abs(long a)");
		//static double 	acos(double a)
		outputTitle("acos(double a)");
		//static double 	asin(double a)
		outputTitle("asin(double a)");
		//static double 	atan(double a)
		outputTitle("atan(double a)");
		//static double 	atan2(double y, double x)
		outputTitle("atan2(double y, double x)");
		//static double 	cbrt(double a)
		outputTitle("cbrt(double a)");
		//static double 	ceil(double a)
		outputTitle("ceil(double a)");
		//static double 	copySign(double magnitude, double sign)
		outputTitle("copySign(double magnitude, double sign)");
		//static float 	copySign(float magnitude, float sign)
		outputTitle("copySign(float magnitude, float sign)");
		//static double 	cos(double a)
		outputTitle("cos(double a)");
		//static double 	cosh(double x)
		outputTitle("cosh(double x)");
		//static double 	exp(double a)
		outputTitle("exp(double a)");
		//static double 	expm1(double x)
		outputTitle("expm1(double x)");
		//static double 	floor(double a)
		outputTitle("floor(double a)");
		//static int 	getExponent(double d)
		outputTitle("getExponent(double d)");
		//static int 	getExponent(float f)
		outputTitle("getExponent(float f)");
		//static double 	hypot(double x, double y)
		outputTitle("hypot(double x, double y)");
		//static double 	IEEEremainder(double f1, double f2)
		outputTitle("IEEEremainder(double f1, double f2)");
		//static double 	log(double a)
		outputTitle("log(double a)");
		//static double 	log10(double a)
		outputTitle("log10(double a)");
		//static double 	log1p(double x)
		outputTitle("log1p(double x)");
		//static double 	max(double a, double b)
		outputTitle("max(double a, double b)");
		//static float 	max(float a, float b)
		outputTitle("max(float a, float b)");
		//static int 	max(int a, int b)
		outputTitle("max(int a, int b)");
		//static long 	max(long a, long b)
		outputTitle("max(long a, long b)");
		//static double 	min(double a, double b)
		outputTitle("min(double a, double b)");
		//static float 	min(float a, float b)
		outputTitle("min(float a, float b)");
		//static int 	min(int a, int b)
		outputTitle("min(int a, int b)");
		//static long 	min(long a, long b)
		outputTitle("min(long a, long b)");
		//static double 	nextAfter(double start, double direction)
		outputTitle("nextAfter(double start, double direction)");
		//static float 	nextAfter(float start, double direction)
		outputTitle("nextAfter(float start, double direction)");
		//static double 	nextUp(double d)
		outputTitle("nextUp(double d)");
		//static float 	nextUp(float f)
		outputTitle("nextUp(float f)");
		//static double 	pow(double a, double b)
		outputTitle("pow(double a, double b)");
		//static double 	random()
		outputTitle("random()");
		//static double 	rint(double a)
		outputTitle("rint(double a)");
		//static long 	round(double a)
		outputTitle("round(double a)");
		//static int 	round(float a)
		outputTitle("round(float a)");
		//static double 	scalb(double d, int scaleFactor)
		outputTitle("scalb(double d, int scaleFactor)");
		//static float 	scalb(float f, int scaleFactor)
		outputTitle("scalb(float f, int scaleFactor)");
		//static double 	signum(double d)
		outputTitle("signum(double d)");
		//static float 	signum(float f)
		outputTitle("signum(float f)");
		//static double 	sin(double a)
		outputTitle("sin(double a)");
		//static double 	sinh(double x)
		outputTitle("sinh(double x)");
		//static double 	sqrt(double a)
		outputTitle("sqrt(double a)");
		//static double 	tan(double a)
		outputTitle("tan(double a)");
		//static double 	tanh(double x)
		outputTitle("tanh(double x)");
		//static double 	toDegrees(double angrad)
		outputTitle("toDegrees(double angrad)");
		//static double 	toRadians(double angdeg)
		outputTitle("toRadians(double angdeg)");
		//static double 	ulp(double d)
		outputTitle("ulp(double d)");
		//static float 	ulp(float f)
		outputTitle("ulp(float f)");


	}

	private static void outputTitle(String title){
		System.out.println("★" + title + "★");
	}

}
