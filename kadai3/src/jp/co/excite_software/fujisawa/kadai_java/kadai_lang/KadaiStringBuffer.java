package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiStringBuffer {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		/* コンストラクタ */

		//StringBuffer()
		outputTitle("StringBuffer()");
		//StringBuffer(CharSequence seq)
		outputTitle("StringBuffer(CharSequence seq)");
		//StringBuffer(int capacity)
		outputTitle("StringBuffer(int capacity)");
		//StringBuffer(String str)
		outputTitle("StringBuffer(String str)");

		/* メソッド */

		//StringBuffer 	append(boolean b)
		outputTitle("append(boolean b)");
		//StringBuffer 	append(char c)
		outputTitle("append(char c)");
		//StringBuffer 	append(char[] str)
		outputTitle("append(char[] str)");
		//StringBuffer 	append(char[] str, int offset, int len)
		outputTitle("append(char[] str, int offset, int len)");
		//StringBuffer 	append(CharSequence s)
		outputTitle("append(CharSequence s)");
		//StringBuffer 	append(CharSequence s, int start, int end)
		outputTitle("append(CharSequence s, int start, int end)");
		//StringBuffer 	append(double d)
		outputTitle("append(double d)");
		//StringBuffer 	append(float f)
		outputTitle("append(float f)");
		//StringBuffer 	append(int i)
		outputTitle("append(int i)");
		//StringBuffer 	append(long lng)
		outputTitle("append(long lng)");
		//StringBuffer 	append(Object obj)
		outputTitle("append(Object obj)");
		//StringBuffer 	append(String str)
		outputTitle("append(String str)");
		//StringBuffer 	append(StringBuffer sb)
		outputTitle("append(StringBuffer sb)");
		//StringBuffer 	appendCodePoint(int codePoint)
		outputTitle("appendCodePoint(int codePoint)");
		//int 	capacity()
		outputTitle("capacity()");
		//char 	charAt(int index)
		outputTitle("charAt(int index)");
		//int 	codePointAt(int index)
		outputTitle("codePointAt(int index)");
		//int 	codePointBefore(int index)
		outputTitle("codePointBefore(int index)");
		//int 	codePointCount(int beginIndex, int endIndex)
		outputTitle("codePointCount(int beginIndex, int endIndex)");
		//StringBuffer 	delete(int start, int end)
		outputTitle("delete(int start, int end)");
		//StringBuffer 	deleteCharAt(int index)
		outputTitle("deleteCharAt(int index)");
		//void 	ensureCapacity(int minimumCapacity)
		outputTitle("ensureCapacity(int minimumCapacity)");
		//void 	getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
		outputTitle("getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)");
		//int 	indexOf(String str)
		outputTitle("indexOf(String str)");
		//int 	indexOf(String str, int fromIndex)
		outputTitle("indexOf(String str, int fromIndex)");
		//StringBuffer 	insert(int offset, boolean b)
		outputTitle("insert(int offset, boolean b)");
		//StringBuffer 	insert(int offset, char c)
		outputTitle("insert(int offset, char c)");
		//StringBuffer 	insert(int offset, char[] str)
		outputTitle("insert(int offset, char[] str)");
		//StringBuffer 	insert(int index, char[] str, int offset, int len)
		outputTitle("insert(int index, char[] str, int offset, int len)");
		//StringBuffer 	insert(int dstOffset, CharSequence s)
		outputTitle("insert(int dstOffset, CharSequence s)");
		//StringBuffer 	insert(int dstOffset, CharSequence s, int start, int end)
		outputTitle("insert(int dstOffset, CharSequence s, int start, int end)");
		//StringBuffer 	insert(int offset, double d)
		outputTitle("insert(int offset, double d)");
		//StringBuffer 	insert(int offset, float f)
		outputTitle("insert(int offset, float f)");
		//StringBuffer 	insert(int offset, int i)
		outputTitle("insert(int offset, int i)");
		//StringBuffer 	insert(int offset, long l)
		outputTitle("insert(int offset, long l)");
		//StringBuffer 	insert(int offset, Object obj)
		outputTitle("insert(int offset, Object obj)");
		//StringBuffer 	insert(int offset, String str)
		outputTitle("insert(int offset, String str)");
		//int 	lastIndexOf(String str)
		outputTitle("lastIndexOf(String str)");
		//int 	lastIndexOf(String str, int fromIndex)
		outputTitle("lastIndexOf(String str, int fromIndex)");
		//int 	length()
		outputTitle("length()");
		//int 	offsetByCodePoints(int index, int codePointOffset)
		outputTitle("offsetByCodePoints(int index, int codePointOffset)");
		//StringBuffer 	replace(int start, int end, String str)
		outputTitle("replace(int start, int end, String str)");
		//StringBuffer 	reverse()
		outputTitle("reverse()");
		//void 	setCharAt(int index, char ch)
		outputTitle("setCharAt(int index, char ch)");
		//void 	setLength(int newLength)
		outputTitle("setLength(int newLength)");
		//CharSequence 	subSequence(int start, int end)
		outputTitle("subSequence(int start, int end)");
		//String 	substring(int start)
		outputTitle("substring(int start)");
		//String 	substring(int start, int end)
		outputTitle("substring(int start, int end)");
		//String 	toString()
		outputTitle("toString()");
		//void 	trimToSize()
		outputTitle("trimToSize()");

	}

	private static void outputTitle(String title){
		System.out.println("★" + title + "★");
	}


}
