package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiLong {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		//int 	bitCount(long i)
		outputTitle("bitCount(long i)");


		//byte 	byteValue()
		outputTitle("bitCount(long i)");
		//int 	compare(long x, long y)
		outputTitle("compare(long x, long y)");
		//int 	compareTo(Long anotherLong)
		outputTitle("compareTo(Long anotherLong)");
		//Long 	decode(String nm)
		outputTitle("decode(String nm)");
		//double 	doubleValue()
		outputTitle("doubleValue()");
		//boolean 	equals(Object obj)
		outputTitle("equals(Object obj)");
		//float 	floatValue()
		outputTitle("	floatValue()");
		//Long 	getLong(String nm)
		outputTitle("getLong(String nm)");
		//Long 	getLong(String nm, long val)
		outputTitle("getLong(String nm, long val)");
		//Long 	getLong(String nm, Long val)
		outputTitle("getLong(String nm, Long val)");
		//int 	hashCode()
		outputTitle("hashCode()");
		//long 	highestOneBit(long i)
		outputTitle("highestOneBit(long i)");
		//int 	intValue()
		outputTitle("intValue()");
		//long 	longValue()
		outputTitle("longValue()");
		//long 	lowestOneBit(long i)
		outputTitle("lowestOneBit(long i)");
		//int 	numberOfLeadingZeros(long i)
		outputTitle("numberOfLeadingZeros(long i)");
		//numberOfTrailingZeros(long i)
		outputTitle("numberOfTrailingZeros(long i)");
		//long 	parseLong(String s)
		outputTitle("parseLong(String s)");
		//long 	parseLong(String s, int radix)
		outputTitle("parseLong(String s, int radix)");
		//long 	reverse(long i)
		outputTitle("reverse(long i)");
		//long 	reverseBytes(long i)
		outputTitle("reverseBytes(long i)");
		//long 	rotateLeft(long i, int distance)
		outputTitle("rotateLeft(long i, int distance)");
		//long 	rotateRight(long i, int distance)
		outputTitle("rotateRight(long i, int distance)");
		//short 	shortValue()
		outputTitle("shortValue()");
		//int 	signum(long i)
		outputTitle("signum(long i)");
		//String 	toBinaryString(long i)
		outputTitle("toBinaryString(long i)");
		//String 	toHexString(long i)
		outputTitle("toHexString(long i)");
		//String 	toOctalString(long i)
		outputTitle("toOctalString(long i)");
		//String 	toString()	outputTitleで使用、割愛

		//String 	toString(long i)
		outputTitle("toString(long i)");
		//String 	toString(long i, int radix)
		outputTitle("toString(long i, int radix)");
		//Long 	valueOf(long l)
		outputTitle("valueOf(long l)");
		//Long 	valueOf(String s)
		outputTitle("valueOf(String s)");
		//Long 	valueOf(String s, int radix)
		outputTitle("valueOf(String s, int radix)");

	}

	private static void outputTitle(String title){
		System.out.println("★" + title + "★");
	}

	private static void output(Long l){
		System.out.println(Long.toString(l));
	}


}
